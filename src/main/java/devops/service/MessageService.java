package devops.service;

import devops.dto.MessagePayloadDto;

/**
 *
 * @author dparedes
 */
public interface MessageService {
    
    public String processMessage(MessagePayloadDto messageDto);
    
}
