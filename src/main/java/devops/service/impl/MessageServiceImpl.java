package devops.service.impl;

import devops.dto.MessagePayloadDto;
import devops.service.MessageService;
import org.springframework.stereotype.Service;

/**
 *
 * @author dparedes
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Override
    public String processMessage(MessagePayloadDto messageDto) {
        if(messageDto.getTo() == null || messageDto.getTo().isEmpty()){
            return "ERROR";
        }
        return "Hello " + messageDto.getTo() + " your message will be send";
    }

}
