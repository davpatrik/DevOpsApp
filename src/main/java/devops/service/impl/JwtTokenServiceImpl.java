package devops.service.impl;

import devops.service.JwtTokenService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

/**
 *
 * @author dparedes
 */
@Service
public class JwtTokenServiceImpl implements JwtTokenService {

    @Override
    public String getJWTToken(String username) {
        String secretKey = "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c";        
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("DevOpsApp")
                .setSubject(username)
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))                
                .setExpiration(new Date(System.currentTimeMillis() + 1000000))                
                .signWith(SignatureAlgorithm.HS256,
                        secretKey.getBytes()).compact();

        return "DevOpsJwtKey " + token;
    }

}
