package devops.service;

import devops.dto.MessagePayloadDto;

/**
 *
 * @author dparedes
 */
public interface JwtTokenService {
    
    public String getJWTToken(String username);
    
}
