package devops.controller;

import devops.dto.UserDto;
import devops.service.impl.JwtTokenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dparedes
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private JwtTokenServiceImpl jwtTokenService;

    @PostMapping("/getJwtToken")
    public UserDto getJwtToken() {
        String username = "ANONYMOUS_2";
        String token = jwtTokenService.getJWTToken(username);
        UserDto userDto = new UserDto();
        userDto.setUser(username);
        userDto.setToken(token);
        return userDto;
    }

}
