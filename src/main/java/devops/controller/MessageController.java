package devops.controller;

import devops.dto.MessagePayloadDto;
import devops.dto.MessageResponseDto;
import devops.service.impl.MessageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dparedes
 */
@RestController
public class MessageController {
       
    @Autowired    
    private MessageServiceImpl messageService;
    
    @PostMapping("/DevOps")
    public MessageResponseDto sendMessage(@RequestBody MessagePayloadDto messageDto) {
        MessageResponseDto messageResponse = new MessageResponseDto(messageService.processMessage(messageDto));
        System.out.println("messageResponse: " + messageResponse.getMessage());
        return messageResponse;
        //return "Hello " + messageDto.getTo();
    }

}
