package devops.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author dparedes
 */
@Getter
@Setter
@NoArgsConstructor
public class MessagePayloadDto {
    
    private String message;
    private String to;
    private String from;
    private Integer timeToLifeSec;    
    
}
