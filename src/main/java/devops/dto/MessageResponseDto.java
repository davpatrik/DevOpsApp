package devops.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dparedes
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class MessageResponseDto {
    
    private String message;    

    public MessageResponseDto(String message) {
        this.message = message;
    }
    
}
