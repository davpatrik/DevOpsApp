package devops.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author dparedes
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDto {
    
    private String user;
    private String password;
    private String token;            
    
}
