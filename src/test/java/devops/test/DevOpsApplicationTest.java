package devops.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DevOpsApplicationTest {

    @Test
    public void contextLoads() {
    }

}
