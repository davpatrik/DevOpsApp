package devops.test.controller;

import devops.controller.MessageController;
import devops.dto.MessagePayloadDto;
import devops.dto.MessageResponseDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageControllerTest {

    @Autowired
    private MessageController messageController;

    @Test
    public void contextLoads() throws Exception {
        System.out.println("TEST MessageControllerTest.contextLoads");
        org.assertj.core.api.Assertions.assertThat(messageController).isNotNull();
    }

    @Test
    public void whenRequestJwtToken_thenReturnCorrectRequest() {
        System.out.println("TEST MessageControllerTest.whenRequestJwtToken_thenReturnCorrectRequest");
        try {
            MessagePayloadDto messageDto = new MessagePayloadDto();
            messageDto.setTo("USER");
            MessageResponseDto messageResponseDto = messageController.sendMessage(messageDto);
            System.out.println("messageResponseDto: " + messageResponseDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
