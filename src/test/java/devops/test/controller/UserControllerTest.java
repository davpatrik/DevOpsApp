package devops.test.controller;

import devops.controller.UserController;
import devops.dto.UserDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author dparedes
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

    @Autowired
    private UserController userController;

    @Test
    public void contextLoads() throws Exception {
        System.out.println("TEST contextLoads");
        org.assertj.core.api.Assertions.assertThat(userController).isNotNull();
    }

    @Test
    public void whenRequestJwtToken_thenReturnCorrectRequest() {
        System.out.println("TEST whenRequestJwtToken_thenReturnCorrectRequest");
        try {
            UserDto userDto = userController.getJwtToken();
            System.out.println("userDto: " + userDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
