FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/DevOpsApp-1.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
