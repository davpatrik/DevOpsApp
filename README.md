# Project description

## DevOpsApp
It is a spring boot project (SpringBoot v2.4.1 + Maven POM 4.0.0 + JWT + JUnit), which provides REST services according to 'English DevOps - Test-2.pdf' document.

## CI/CD configurations
DevOpsApp contains file [.gitlab-ci.yml] which is the pipe line configuration file for gitLab, it contains the following stages:
- build
- test: Run tests for ensure code quality
- package: Build and push docker image into [GitHub repository](https://hub.docker.com/repository/docker/davpatrik/devops-sample)
- deploy: Deploy the image form [GitHub repository](https://hub.docker.com/repository/docker/davpatrik/devops-sample) into a Kubernetes Cluster (AKS) configured from scratch in Azure Cloud Services. It depends of file '/kubernetes/deployment.yml'

## Software requirements
It is required to previous install:
-jdk1.8.0_211: [Instalación guide](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/).
-Git: [Instalación guide](https://github.com/git-guides/install-git#:~:text=To%20install%20Git%2C%20navigate%20to,installation%20by%20typing%3A%20git%20version%20.).
-Maven: [Instalación guide](https://www.mkyong.com/maven/how-to-install-maven-in-windows/).
-Docker: [Instalación guide](https://runnable.com/docker/install-docker-on-linux).

# Project download and execution

## Download DevOpsApp
Download sources at GitHub [https://gitlab.com/davpatrik/DevOpsApp.git](https://gitlab.com/davpatrik/DevOpsApp.git). 

## Running DevOpsApp
Run project in IDE: 
-In Eclipse: selectProject->RunAs->3 Spring Boot App, or 
-In NetBeans: selectProject->buildWhitDependencies then selectProject->Run 
Or in command line, in CMD first go to main project folder "..\DevOpsApp" and execute the following command:
```bash
mvn spring-boot:run
```
# Project testing

## Get jwtToken form DevOpsApp
After project is running, try POST in the next URL in postman-> http://localhost:8080/user/getJwtToken, send nothing, the result must be:
```bash
{
    "user": "ANONYMOUS",
    "password": null,
    "token": "DevOpsJwtKey eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJEZXZPcHNBcHAiLCJzdWIiOiJBTk9OWU1PVVMiLCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwiaWF0IjoxNjA4MTc2NjI4LCJleHAiOjE2MDgxNzc2Mjh9.NNpoiiMoy8uD_sw22BUpc1EWuZQAZ7y7mWqJlJply4g"
}
```

## Use jwtToken + ApiKey to consume principal REST service DevOpsApp
Try POST in the next URL in postman-> http://localhost:8080/DevOps, send the next params

### Header params
```bash
Content-Type: application/json
X-JWT-KWY: $token
X-Parse-REST-API-Key: 2f5ae96c-b558-4c7b-a590-a501ae1c3f6c
```
### Body params
```bash
{
    "message": "This is a test",
    "to": "David.Paredes",
    "from": "Rita Asturia",
    "timeToLifeSec": 45
}
```

Or simply try CURL
```bash
curl -X POST \
-H "X-Parse-REST-API-Key: 2f5ae96c-b558-4c7b-a590-a501ae1c3f6c" \
-H "X-JWT-KWY: $token" \
-H "Content-Type: application/json" \
-d '{ "message" : "This is a test", "to": "David.Paredes", "from": "Rita Asturia", "timeToLifeSec" : 45 }' \
http://localhost:8080/DevOps
```

The result should be:
```bash
{
    "message": "Hello David.Paredes your message will be send"
}
```
## //TODO
- Add SONAR dependencies for static code revision
- Enable 'dockerd' in AKS Cluster for pull commands to be successfully excecuted, in order to avoid error:
```bash
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
Cleaning up file based variables
ERROR: Job failed: command terminated with exit code 1
```
